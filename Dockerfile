FROM openjdk:8-jdk-alpine

ARG JAR_FILE=target/produto-*.jar

WORKDIR /appagent
COPY /appagent/ ./

WORKDIR /app
COPY ${JAR_FILE} ./api-produto.jar

CMD ["java","-javaagent:/appagent/javaagent.jar","-jar","/app/api-produto.jar"]
