package br.com.itau.tarifas.produto.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.ALREADY_REPORTED, reason = "Produto já cadastrado")
public class ProdutoJaCadastradoException extends RuntimeException  {
}
