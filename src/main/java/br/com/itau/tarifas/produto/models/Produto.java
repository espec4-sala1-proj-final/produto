package br.com.itau.tarifas.produto.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(unique = true)
    @NotNull(message = "Nome deve ser informado")
    @NotBlank(message = "Nome não pode ser vazio")
    @Size(min=3, message = "Nome deve ter pelo menos 3 caracteres")
    private String nome;

    @NotNull(message = "Sigla deve ser informado")
    @NotBlank(message = "Sigla não pode ser vazio")
    @Size(min=2, max=3, message = "Sigla deve ter 2 ou 3 caracteres")
    private String sigla;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public Produto() {
    }
}
