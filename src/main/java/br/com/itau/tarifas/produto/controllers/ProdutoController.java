package br.com.itau.tarifas.produto.controllers;

import br.com.itau.tarifas.produto.models.Produto;
import br.com.itau.tarifas.produto.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")
public class ProdutoController
{
    @Autowired
    ProdutoService produtoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto incluir(@RequestBody @Valid Produto produto)
    {
        return  produtoService.incluir(produto);
    }

    @GetMapping
    public Iterable<Produto> buscar()
    {
        return produtoService.buscartodos();
    }

    @GetMapping("/{Id}")
    public Produto buscarPorId(@PathVariable(name = "Id") Integer id)
    {
        return produtoService.buscarPorId(id);
    }
}
