package br.com.itau.tarifas.produto.services;

import br.com.itau.tarifas.produto.exceptions.ProdutoJaCadastradoException;
import br.com.itau.tarifas.produto.exceptions.ProdutoNaoEncontradoException;
import br.com.itau.tarifas.produto.models.Produto;
import br.com.itau.tarifas.produto.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService
{
    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    private ProdutoService produtoService;

    public Produto incluir(Produto investimento)
    {
        System.out.println("Produto - Tentativa de Inclusão");
        Optional<Produto> produto = produtoRepository.findByNome(investimento.getNome());
        if(!produto.isPresent())
        {
            Produto produtoIncluido = produtoRepository.save(investimento);
            System.out.println("Produto - Inclusão de Produto " + produtoIncluido.getNome() + " efetuada com sucesso");
            return produtoIncluido;
        }
        else
        {
            System.out.println("Produto - Erro ao incluir produto: Produto já cadastrado - " + investimento.getNome());
            throw new ProdutoJaCadastradoException();
        }
    }

    public Iterable<Produto> buscartodos()
    {
        System.out.println("Produto - Buscar Todos");
        return produtoRepository.findAll();
    }

    public Produto buscarPorId(Integer id)
    {
        System.out.println("Produto - Buscar Por Id");
        Optional<Produto> produto = produtoRepository.findById(id);

        if(produto.isPresent())
        {
            System.out.println("Produto - Encontrou o produto " + produto.get().getNome());
            return produto.get();
        }
        else
        {
            System.out.println("Produto - Erro ao buscar produto: Produto não encontrado - " + id);
            throw new ProdutoNaoEncontradoException();
        }
    }
}
