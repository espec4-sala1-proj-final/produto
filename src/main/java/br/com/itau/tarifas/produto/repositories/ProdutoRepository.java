package br.com.itau.tarifas.produto.repositories;

import br.com.itau.tarifas.produto.models.Produto;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ProdutoRepository extends CrudRepository<Produto, Integer>
{
    Optional<Produto> findByNome(String Nome);
}
